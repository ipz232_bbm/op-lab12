#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main() {
	srand(time(0));

	const int N = 10;
	int arr[N], lokMax = 100;

	for (int i = 0; i < N; i++) {
		arr[i] = -100 + rand() % 201;
		printf("%d ", arr[i]);
	}

	for (int i = 1; i < N - 1; i++) {
		if (arr[i] > arr[i - 1] && arr[i] > arr[i + 1]) {
			if (arr[i] < lokMax) lokMax = arr[i];
		}
	}
	printf("\nminLokMax = %d", lokMax);

	return 0;
}