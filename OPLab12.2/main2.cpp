#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <windows.h>
#include <time.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));

	int N;
	do {
		printf("N = ");
		scanf("%d", &N);
	} while (N <= 0 || N > 15);

	int A[15], B[7];

	for (int i = 0; i < N; i++) {
		A[i] = -100 + rand() % 201;
		printf("%d ", A[i]);
	}

	printf("\n����� ������ B = 7\n");

	for (int i = 0; i < N / 2; i++) {
		B[i] = A[2 * i + 1];
		printf("%d ", B[i]);
	}

	return 0;
}