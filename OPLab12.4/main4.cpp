#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main() {
	srand(time(0));

	const int Lsize = 11;
	int L[Lsize], Lsorted[Lsize], k = 0;

	for (int i = 0; i < Lsize; i++) {
		L[i] = -100 + rand() % 201;
		printf("%d ", L[i]);

		if (L[i] > 0) {
			Lsorted[k] = L[i];
			k++;
		}

	}

	printf("\n----------------------------------------\n");

	for (int i = 0; i < Lsize; i++) {
		if (!(L[i] > 0)) {
			Lsorted[k] = L[i];
			k++;
		}
		printf("%d ", Lsorted[i]);

	}

	return 0;
}