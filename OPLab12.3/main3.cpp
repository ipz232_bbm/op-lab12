#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <windows.h>
#include <time.h>

int main() {
	srand(time(0));

	const int N = 15;
	int arr[N], temp;

	for (int i = 0; i < N; i++) {
		arr[i] = -100 + rand() % 201;
		printf("%3d | ", arr[i]);
	}

	temp = arr[0];

	for (int i = 0; i < N - 1; i++) {
		arr[i] = arr[i + 1];
	}
	arr[N - 1] = temp;

	printf("\n");

	for (int i = 0; i < N; i++) {
		printf("%3d | ", arr[i]);
	}

	return 0;
}