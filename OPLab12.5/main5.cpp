#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

int main() {
	srand(time(0));

	const int Fsize = 14;
	int F[Fsize], Fsorted[Fsize], k = 0, first;

	for (int i = 0; i < Fsize; i++) {
		F[i] = -100 + rand() % 201;
		printf("%d ", F[i]);
	}
	first = F[0];

	printf("\n----------------------------------------\n");

	for (int i = 0; i < Fsize; i++) {
		if (F[i] > first) {
			Fsorted[k] = F[i];
			k++;
		}
	}
	for (int i = 0; i < Fsize; i++) {
		if (F[i] <= first) {
			Fsorted[k] = F[i];
			k++;
		}
		printf("%d ", Fsorted[i]);
	}

	return 0;
}